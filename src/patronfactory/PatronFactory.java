package patronfactory;

/**
 *
 * @author Avila
 */
public class PatronFactory {


    public static void main(String[] args) {
        
        //ConcreteCreator
        Moneda Pesos = FactoryMonedas.CrearMoneda("MEXICO");
        System.out.println("Simbolo Moneda: "+Pesos.obtenerSimbolo());
        
        Moneda Dolar = FactoryMonedas.CrearMoneda("USA");
        System.out.println("Simbolo Moneda: "+Pesos.obtenerSimbolo());
    }
    
}
