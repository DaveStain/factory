/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package patronfactory;

/**
 *
 * @author Avila
 */

//Creator
public class FactoryMonedas {
    
    public static Moneda CrearMoneda (String pais){
       if (pais.equalsIgnoreCase("MEXICO")){
               return new Pesos();
       }
       if (pais.equalsIgnoreCase("USA")){
               return new Dolares();
       }
        throw new IllegalArgumentException("No existe la moneda");
    }    
}
